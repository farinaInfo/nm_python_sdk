from setuptools import setup, find_packages
setup(
    name="nm",
    version="5.0",
    author="rahul pandit",
    author_email="rahulpandit@gmail.com",
    url="https://bit.ly/edeediong-resume",
    description="An application that informs you of the time in different locations and timezones",
    packages=find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=["pandas"]
)